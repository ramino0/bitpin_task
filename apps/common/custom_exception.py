from rest_framework import status as st

from .response_message import ResponseMessage


class RequestValidationException(Exception):
    errors: list

    def __init__(self, errors: list):
        self.errors = errors


class ServiceCustomException(Exception):
    message: ResponseMessage
    status: st

    def __init__(self, message: ResponseMessage, status: st):
        self.message = message
        self.status = status
